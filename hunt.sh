#!/bin/sh

echo "Setup script for Treasure hunt event"
echo "Enter the clue to be displayed by this node: "
CLUE=""
read CLUE
echo "The clue is: $CLUE"

echo "Updating indexes..."
pkg update

echo "Installing nginx..."
pkg install nginx

echo "Downloading website resources..."
curl -o web.tar.gz https://skran.codeberg.page/web.tar.gz

echo "Extracting..."
mkdir web
tar xf web.tar.gz -C web

echo "Inserting clue..."
cat web/page.html | sed "s/CLUE/$CLUE/g" > web/index.html

echo "Copying files to nginx html folder"
cp web/* ~/../usr/share/nginx/html/

echo "Done"
echo "To start the server, run the command 'nginx'"
echo "It can be accessed via entering IP_ADDRESS:8080 via the browser of any device in the same WiFi/Hotspot network"
echo "IP address of the webpage can be obtained from the 'Gateway' section in WiFi settings of any device in same WiFi/Hotspot"
